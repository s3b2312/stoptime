<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInShoppingCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('in_shopping_carts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('shopping_cart_id')->index('id_shopping');
			$table->integer('articulo_id');
			$table->string('crema', 100)->nullable();
			$table->string('azucar', 100)->nullable();
			$table->string('servir', 100)->nullable();
			$table->string('insumos')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('in_shopping_carts');
	}

}
