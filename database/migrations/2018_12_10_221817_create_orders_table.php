<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('shopping_id')->index('shopping_id');
			$table->integer('user_id')->unsigned()->nullable()->index('user_id');
			$table->string('line1', 120)->nullable();
			$table->string('line2', 120)->nullable();
			$table->string('city', 120)->nullable();
			$table->string('postal_code', 120)->nullable();
			$table->string('country_code', 120)->nullable();
			$table->string('state', 120)->nullable();
			$table->string('recipient_name', 120);
			$table->string('email', 120);
			$table->string('status', 120)->default('creado');
			$table->string('guide_number', 120)->nullable();
			$table->decimal('total', 11);
			$table->integer('pagado')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
