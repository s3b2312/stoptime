<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShoppingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shopping', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('status', 150);
			$table->string('customid', 120)->nullable()->unique('customid');
			$table->dateTime('fecha_reserva')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shopping');
	}

}
