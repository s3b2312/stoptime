<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToArticulosInsumosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('articulos_insumos', function(Blueprint $table)
		{
			$table->foreign('articulo_id', 'articulos_insumos_ibfk_1')->references('id')->on('articulo')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('insumo_id', 'articulos_insumos_ibfk_2')->references('id')->on('insumos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('articulos_insumos', function(Blueprint $table)
		{
			$table->dropForeign('articulos_insumos_ibfk_1');
			$table->dropForeign('articulos_insumos_ibfk_2');
		});
	}

}
