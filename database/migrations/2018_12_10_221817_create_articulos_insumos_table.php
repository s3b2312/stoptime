<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticulosInsumosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articulos_insumos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('articulo_id')->index('articulo_id');
			$table->integer('insumo_id')->index('insumo_id');
			$table->integer('cant_porcion')->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articulos_insumos');
	}

}
