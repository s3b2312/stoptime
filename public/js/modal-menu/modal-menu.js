

		$modalBottom = document.querySelectorAll('.modal-bottom');

		for(var i = 0; i < $modalBottom.length ; i++){
			$modalBottom[i].addEventListener('click' ,(e, $modalBottom)=>{
				$checkbox.run(e.target.id);
			} , false);			
		}	

		var $checkbox = {
			checkbox: null ,
			cantInsumos: null,
			cantInsumosInicial: null,
			run: (idModal)=>{
				$checkbox.checkbox = document.querySelectorAll( '.checkbox-modal-' + idModal );
				$checkbox.cantInsumos = document.querySelectorAll( '.cantInsumos-modal-' + idModal);

				$checkbox.cantInsumosInicial = $checkbox.cantInsumos['0'].innerText;

				for(var i = 0 ; i < $checkbox.checkbox.length ; i++){
					$checkbox.checkbox[i].addEventListener('change', $checkbox.aEL , false);
				}
			},
			catInsumosMenos: ()=>{
				$checkbox.cantInsumos['0'].innerText = parseInt($checkbox.cantInsumos['0'].innerText) - 1;

				if($checkbox.cantInsumos['0'].innerText == 0){
					$checkbox.desabilitarCheckBox();
				}
			},
			catInsumosMas: ()=>{
				$checkbox.cantInsumos['0'].innerText = parseInt($checkbox.cantInsumos['0'].innerText) + 1;

				if($checkbox.cantInsumos['0'].innerText > 0){
					$checkbox.habilitarCheckBox();
				}
			},
			desabilitarCheckBox: ()=>{
				for(var i = 0 ; i < $checkbox.checkbox.length ; i++){
					if(!$checkbox.checkbox[i].checked){
						$checkbox.checkbox[i].disabled = true;
					}
				}
			},
			habilitarCheckBox: ()=>{
				for(var i = 0 ; i < $checkbox.checkbox.length ; i++){
					$checkbox.checkbox[i].disabled = false;
				}
			},
			habilutarBodyModal:()=>{
				$checkbox.cantInsumos['0'].innerText = $checkbox.cantInsumosInicial;

				for(var i = 0 ; i < $checkbox.checkbox.length ; i++){
					$checkbox.checkbox[i].removeEventListener('change', $checkbox.aEL , false);
					$checkbox.checkbox[i].disabled = false;
					$checkbox.checkbox[i].checked = false;
				}
			},
			aEL:(e)=>{
				
				if(e.target.checked){
					$checkbox.catInsumosMenos(e);
				}else{
					$checkbox.catInsumosMas(e);
				}
			},
			

			};

		
