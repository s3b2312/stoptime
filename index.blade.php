@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h3>Menu</h3>
			@include('menu.search')
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<center>{{$articulos->render()}}

				<div class="containerr">

					@foreach($articulos as $art)
					<div class="item">
						<div class="sub-item">
							<div class="nombre">{{ $art->nombre}}</div>
							<img src="{{asset('imagenes/articulos/'.$art->imagen)}}" alt="{{$art->imagen}}" height="100px" width="100px" class=" imagen">
						</div>
					</div>
					@endforeach
				</div>
			
			{{$articulos->render()}}</center>
		</div>
	</div>
@endsection