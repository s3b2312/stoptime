<?php

namespace StopTime;

use Illuminate\Database\Eloquent\Model;

class Insumo extends Model
{
    protected $fillable = [
    	'nombre',
    	'stock',
    	'descripcion'
    ];



    public function scopeOrderNombre($query){

        return $query->orderBy('nombre');
    }

    public function articulos(){
        return $this->belongsToMany('StopTime\Articulo');
    }
}
