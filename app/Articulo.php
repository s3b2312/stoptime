<?php

namespace StopTime;

use Illuminate\Support\Facades\Storage; //eliminar imagen
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use StopTime\Insumo;
use Image;

class Articulo extends Model
{
    protected $table = 'articulo'; //nombre de la tabla

    protected $primaryKey = 'id'; //primarykay campo

    public $timestamps = false; // si queremos los dos campos extras de fecha de modificacion y creacion

    protected $fillable = [ //campos que agremamos al modelo
    	'nombre',
    	'descripcion',
        'cant_insumos',
        'crema',
        'azucar',
    	'imagen',
    	'stock',
        'valor',
    	'id_categoria',
    	'estado'
    ];

    protected $guarded = [ //campos que no agremamos al modelo
    	
    ];

    //relaciones)

    public function categoria(){
        return $this->belongsTo('StopTime\Categoria' , 'id_categoria');
    }

    public function insumos(){
        return $this->belongsToMany('StopTime\Insumo' , 'articulos_insumos');
    }

    public function shopping_carts(){
        return $this->belongsToMany('StopTime\ShoppingCart', 'in_shopping_carts')->withPivot(['crema', 'azucar', 'servir']);
    }


    //scope




    //general

    public static function stateOn(){
        Articulo::stateOn();
    }

    public function scopeStateOn($query){
        return $query->where('estado', '=', 1);
    }

    public static function articuloEnriquesido($articulos){

        foreach ($articulos as $key => $articulo) {
            $articulo['insumos'] = $articulo->insumos()->get();
        }

        return $articulos;
    }

    public static function chekedInsumos($articulo){

        $insumos = Insumo::orderNombre()->get();
        $insumos_del_articulo = $articulo->insumos()->get();

        $indice_insumos_del_articulo = array();

        foreach ($insumos_del_articulo as  $insumo_del_articulo) {
             array_push($indice_insumos_del_articulo, $insumo_del_articulo->id);
        }

        foreach ($insumos as $insumo) {
            if(in_array($insumo->id, $indice_insumos_del_articulo)){
                $insumo['marcado'] = true;
            }else{
                $insumo['marcado'] = false;
            }
        } 
        return $insumos;
    }


    public static function storageImagen($request , $articulo){
            if($request->get('image-data')){
                $imagenData = $request->get('image-data');
                $imageninfo = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imagenData));
                $file = Image::make($imageninfo);
                $extencion = preg_replace('#image/#' , '', $file->mime());

                $nombre_file = $request->get('nombre').".".$extencion;
                $file -> save(public_path().'/imagenes/articulos/'.$nombre_file);
                $articulo ->imagen = $nombre_file;

                //-----------ORIGINAL
                $nombre_fileOriginal = "Original-".$nombre_file;
                $nombre_anteriorFileOriginal =  "Original-".$request->get('imagen_anterior');

                Articulo::storeOrChangeNameImagenOriginal($request , $nombre_anteriorFileOriginal, $nombre_fileOriginal);

               
                //-----------ICO

                $nombre_fileIco = "Ico-".$nombre_file;
                $file->resize(60,60)->save(public_path().'/imagenes/articulos/'.$nombre_fileIco);
            }
    }

    public static function storeOrChangeNameImagenOriginal($request , $nombre_anterior, $nombre_actual){
        if($nombre_anterior != $nombre_actual){
             if($request->hasFile('imagen')){
                Articulo::storeImagenOriginal($nombre_actual);
            }else{
                Articulo::changeNameImagenOriginal($nombre_anterior,$nombre_actual);
            }
            Articulo::deleteImagen($request->get('imagen_anterior'));
        }
    }

    public static function storeImagenOriginal($nombre_actual){
        $fileOriginal = Input::file('imagen');
        $fileOriginal -> move(public_path().'/imagenes/articulos/', $nombre_actual);
    }

    public static function changeNameImagenOriginal($nombre_anterior , $nombre_actual){
        if(Storage::disk('imagenes_articulos')->exists($nombre_anterior)){
            $old_url = $nombre_anterior;
            $new_url = $nombre_actual;
            Storage::disk('imagenes_articulos')->move( $old_url , $new_url );
            Articulo::deleteImagen($nombre_anterior);

        }else{
            dd("no encontro la imagen");
        }
    }

    public static function deleteImagen($nombre_imagen){
        Storage::disk('imagenes_articulos')->delete($nombre_imagen);
        Storage::disk('imagenes_articulos')->delete("Ico-".$nombre_imagen);
    }


    public function paypalItem(){

        return \PaypalPayment::item()->setName($this->nombre)
                                ->setDescription($this->descripcion)
                                ->setCurrency('USD')
                                ->setQuantity(1)
                                ->setPrice($this->precio / ShoppingCart::actualUS());
    } 

    public function precioUS(){
        $redondeo =round( $this->precio / ShoppingCart::actualUS() , 2); //redondeo a 2 decimales
        return $redondeo;
    } 

    public function reducirStock($cantidad_reducir = 1){
        if ($this->stock >= $cantidad_reducir){
            $this->stock -= $cantidad_reducir;
            return $this->save();
        }else{
            return false;
        }
    }
}
