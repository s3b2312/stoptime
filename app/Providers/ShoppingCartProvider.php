<?php

namespace StopTime\Providers;

use Illuminate\Support\ServiceProvider;
use StopTime\ShoppingCart;

class ShoppingCartProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view){

        $shopping_cart_id = \Session::get('shopping_cart_id');

        $shopping_cart = ShoppingCart::findOrCreateBySessionID($shopping_cart_id);

        \Session::put('shopping_cart_id', $shopping_cart->id);

        $view->with('shoppingCount', $shopping_cart->articulosSize()); 
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
