<?php

namespace StopTime;

use Illuminate\Database\Eloquent\Model;

class InShoppingCart extends Model
{
    protected $table = 'in_shopping_carts'; //nombre de la tabla

    protected $primaryKey = 'id'; //primarykay campo

    public $timestamps = true; // si queremos los dos campos extras de fecha de modificacion y creacion

    protected $fillable = [ //campos que agremamos al modelo
    	'shopping_cart_id',
    	'articulo_id',
        'crema',
        'azucar',
        'servir',
        'insumos'
    ];

    protected $guarded = [ //campos que no agremamos al modelo
    	
    ];

    public static function deleteIDArticulo($articulo_id , $shopping_cart_id ){
        
        return InShoppingCart::where([
            ["articulo_id", $articulo_id],
            ["shopping_cart_id" , $shopping_cart_id]
        ])->first()->delete();
    }

    public static function productsCount($shopping_cart_id){
        return InShoppingCart::where("shopping_cart_id" , $shopping_cart_id)
                                ->count();
    }
}
