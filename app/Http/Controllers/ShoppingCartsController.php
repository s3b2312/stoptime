<?php

namespace StopTime\Http\Controllers;

use StopTime\ShoppingCart;
use StopTime\Order;
use Illuminate\Http\Request;
use StopTime\PayPal;

class ShoppingCartsController extends Controller
{
    public function __construct(){
        $this->middleware('shoppingcart');
    }

    public function index(Request $request)
    {

        $shopping_cart = $request->shopping_cart;

        $articulos = $shopping_cart->articulos()->get();

        $total = $shopping_cart->total();

        return view('shopping_carts.index' , compact('articulos','total'));


    }

    
    public function show(ShoppingCart $shoppingCart, $id)
    {
        $shopping_cart = ShoppingCart::where('customid', $id)->first();
        $articulos = $shopping_cart->articulos()->get();
        $total = $shopping_cart->total();
        $order = $shopping_cart->order();

        return view('shopping_carts.completed' , compact('shopping_cart' , 'order' , 'articulos' , 'total'));
    }

   public function checkout(Request $request){

        $shopping_cart = $request->shopping_cart;
        $shopping_cart->actualizarFechaReserva($request->fecha_reserva);

        $paypal = new Paypal($shopping_cart);
        $payment = $paypal->generate();

        return redirect($payment->getApprovalLink()); 
   }


   public function misCompras(){

        $orders = Order::findAllByIdUser();
        $shopping_carts = Order::ordersToShopingCarts($orders);

        return view('shopping_carts.mis_compras' , compact('shopping_carts' , 'orders'));
   }
}
