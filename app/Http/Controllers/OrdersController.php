<?php

namespace StopTime\Http\Controllers;

use Illuminate\Http\Request;
use StopTime\Order;
use StopTime\ShoppingCart;
use Illuminate\Support\Facades\Redirect;

class OrdersController extends Controller
{

    public function index(Request $request)
    {

        
        $month = $request->month;
        $year = $request->year;

        $orders = Order::latest($month , $year)->get();

        $totalMonthUS = Order::totalMonthUS($month , $year);
        $totalMonthCLI = Order::totalMonthCLI($month , $year);
        $totalMonthCount = Order::totalMonthCount($month , $year);

        
        return view('order.index' , compact('orders', 'totalMonthCLI' ,'totalMonthUS','totalMonthCount'));  
    }

    public function create(Request $request){

        $shopping_cart = ShoppingCart::findOrCreateBySessionID($request->session()->get('shopping_cart_id'));
        $fecha_reserva = $request->fecha_reserva;
        if(Order::createFromReserva($shopping_cart , $fecha_reserva ) == false){
            return Redirect::to('/carrito');
        }else{
            return Redirect::to('/compras/'.$shopping_cart->customid);
        }
    }

    public function store(Request $request){

        return "store Order";

       // return Redirect::to('almacen/articulo')->with('info', 'Orden guardado con éxito');
    }

    public function show(Order $order){

        $articulos = $order->shopping->articulos;

        return view('order.show', compact('order' , 'articulos'));
    }
   
    public function update(Request $request, $id){

        $order = Order::find($id);

        $field = $request->name;

        $order->$field = $request->value;
        $order->save();

        if($request->ajax()){
            return response()->json([
                
            ]);
        }

        return back(); 
    }


    public function destroy(Order $order){

        $order->destroyOrder($order);

        return Redirect::to('orders');
    }

    public function orderDay(){
 
    $orders = Order::latestDay()->get();
   
    return view('order.order_day' , compact('orders'));  

    }

}
