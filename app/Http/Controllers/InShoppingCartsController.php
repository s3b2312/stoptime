<?php

namespace StopTime\Http\Controllers;

use Illuminate\Http\Request;
use StopTime\InShoppingCart;
use StopTime\ShoppingCart;

class InShoppingCartsController extends Controller
{
   
    public function __construct(){ 
        $this->middleware('shoppingcart');
    }
  
   
    public function store(Request $request)
    {
        $shopping_cart = $request->shopping_cart;
        $articulos_id = $request->articulos_id;

        $crema = $request->crema;
        $azucar = $request->azucar;
        $servir = $request->servir;

        $insumos = $request->insumos;//array

        dd($insumos);
        $stringInsumos = '';

        if(!empty($insumos) || $insumos != null){
            foreach ($insumos as $insumo) {
                $stringInsumos =  $stringInsumos." ".$insumo;    
            }
        }

        $respuesta = InShoppingCart::create([
            'shopping_cart_id' => $shopping_cart->id ,
            'articulo_id'=> $articulos_id,
            'crema'=> $crema,
            'azucar'=> $azucar,
            'servir'=> $servir,
            'insumos' => $stringInsumos
        ]); 

        if($request->ajax()){
            return response()->json([
                'products_count' => InShoppingCart::productsCount($shopping_cart->id)
            ]);
        }

        return back();  
    }


    public function destroy($articulo_id , Request $request) 
    {
        $shopping_cart = $request->shopping_cart;
        $shopping_cart_id = $shopping_cart->id;

        $articuloShopping = InShoppingCart::deleteIDArticulo($articulo_id, $shopping_cart_id);

        if($request->ajax()){
            return response()->json([
                'articulo_id' => $articulo_id,
                'shopping_cart_id' => $shopping_cart_id,
                'articuloShopping' => $articuloShopping,
                'products_count' => InShoppingCart::productsCount($shopping_cart_id)
            ]);
        }

        return back();
    }
}
