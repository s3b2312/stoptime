<?php

namespace StopTime\Http\Controllers\Auth;

use StopTime\User;
use StopTime\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data2 = array(
                'name' => $data['name-register'],
                'email-register' => $data['email-register'],
                'password-register' => $data['password-register'],
                'password-register_confirmation' => $data['password-register_confirmation'],
                );


        return Validator::make($data2, [
            'name' => 'required|string|max:255',
            'email-register' => 'required|string|email|max:255|unique:users,email',
            'password-register' => 'required|string|min:6|confirmed',
            'password-register_confirmation' => 'required',
        ],
        [
            'password-register.min' => 'El campo password debe contener al menos 6 caracteres.',
            'password-register.required' => 'El campo password es requerido.',
            'password-register.confirmed' => 'El campo password debe confirmarse.',
            'password-register_confirmation.required' => 'El campo confiormacion password es requerido.',

            'email-register.max' => 'El campo email debe contener al maximo 255 caracteres.',
            'email-register.required' => 'El campo email es requerido.',
            'email-register.email' => 'El campo email tiene que tener formato de email.',


        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \StopTime\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name-register'],
            'email' => $data['email-register'],
            'password' => bcrypt($data['password-register']),
        ]);
    }
}
