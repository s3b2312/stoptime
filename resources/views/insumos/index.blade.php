@extends ('layouts.admin')
@section ('contenido')

<style type="text/css">
	
	.table-resp{
		min-width: 600px;
	}

</style>

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block">
		{!! link_to('/', '', ['class' => 'btn-atras']) !!}
		<h3>Listado de Insumos 
			
				<a href="insumos/create"><button class="btn btn-success btn-success-crear">Nuevo</button></a>
			
		</h3>
		@include('insumos.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block">
		<div class="table-resp-cont">
			<table class="table-resp">
				<thead>
					<th>Nombre</th>
					<th>Stock</th>
					<th>Descripcion</th>
					<th>Acciones</th>
				</thead>
				@foreach($insumos as $insumo)
				<tr>
					<td class="capitalize">{{ $insumo->nombre}}</td>
					<td>{{ $insumo->stock}}</td>
					<td class="texto-largo-cont">
						<div class="texto-largo">
							{{ $insumo->descripcion}}
						</div>
					</td> 
					<td><div>
						
							<a href="{{URL::action('InsumoController@edit' , $insumo->id)}}"><button class="btn btn-info">Editar</button></a>
			
				
						
							<a href="" data-target ="#modal-delete-{{$insumo->id}}" data-toggle = "modal"><button class="btn btn-danger">Eliminar</button></a>
						
						</div>
					</td>
				</tr>
				@include('insumos.modal')
				@endforeach
			</table>

		</div>
		{{$insumos->render()}}</center>
	</div>
</div>
	
@endsection