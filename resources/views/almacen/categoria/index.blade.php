@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block">
			<div class="alert-atras">
                    {!! link_to('/', '', ['class' => 'btn-atras']) !!}
                    @if (session('info'))
                        <div class="alert alert-success">{{ session('info') }}</div>
                    @endif
            </div>
			<h3>Listado de Categorias 
				@can('almacen.categoria.create')
				<a href="categoria/create"><button class="btn btn-success btn-success-crear">Nuevo</button></a></h3>
				@endcan

			{{-- {!! link_to('/almacen/categoria', 'Atras', ['class' => 'btn btn-default']) !!} --}}
			@include('almacen.categoria.search')
		</div>
	</div>

	<div class="cont-table-resp">
		<table class="table-resp" style="min-width: 650px;">
			<thead>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>DESCRIPCION</th>
				<th>OPCIONES</th>
			</thead>
			@foreach($categorias as $cat)
			<tr>
				<td>{{ $cat->id}}</td>
				<td>{{ $cat->nombre}}</td>
				<td>{{ $cat->descripcion}}</td>
				<td>
					
					@can('almacen.categoria.edit')
						<a href="{{URL::action('CategoriaController@edit' , $cat->id_categoria)}}"><button class="btn btn-info">Editar</button></a>
					@endcan

					@can('almacen.categoria.delete')
						<a href="" data-target ="#modal-delete-{{$cat->id_categoria}}" data-toggle = "modal"><button class="btn btn-danger">Eliminar</button></a>
					@endcan
				</td>
			</tr>
			@include('almacen.categoria.modal')
			@endforeach
		</table>
	</div>

@endsection