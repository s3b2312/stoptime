@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block">
			{!! link_to('/', '', ['class' => 'btn-atras']) !!}
			<h3>Listado de Articulos 
				@if($cant_insumos>0)
					@can('almacen.articulo.create')
						<a href="articulo/create"><button class="btn btn-success btn-success-crear">Nuevo</button></a>
					@endcan
				@else
					<a href="insumos/create"><button class="btn btn-success btn-success-crear">Nuevo</button></a>
					<p>sin insumos no puedes crear articulos</p>
				@endif
			</h3>
			@include('almacen.articulo.search')
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block">
			<center>{{$articulos->render()}}
			<div class="table-resp-cont">
				<table class="table-resp">
					<thead>
						<th>ID</th>
						<th>NOMBRE</th>
						<th>CATEGORIA</th>
						<th>STOCK</th>
						<th>CANT INSUMOS</th>
						<th>CREMA</th>
						<th>AZUCAR</th>
						<th>PRECIO</th>
						<th>IMAGEN</th>
						<th>DESCRIPCION</th>
						<th>OPCIONES</th>
					</thead>
					@foreach($articulos as $art)
					<tr>
						<td>{{ $art->id}}</td>
						<td class="capitalize">{{ $art->nombre}}</td>
						<td>{{ $art->categoria->nombre}}</td>
						<td>{{ $art->stock}}</td>
						<td>{{ $art->cant_insumos}}</td>
						<td>{{ ($art->crema == 0)?'NO':'SI'}}</td>
						<td>{{ ($art->azucar== 0)?'NO':'SI'}}</td>
						<td>${{ $art->precio}}</td>
						<td><div class="imagen-tabla">
								<img src="{{asset('imagenes/articulos/Ico-'.$art->imagen)}}" alt="{{$art->imagen}}" class="img-thumbnail">  
							</div>
						</td>
						<td class="texto-largo-cont"><div class="texto-largo">{{ $art->descripcion}}</div>
						</td> 
						<td><div>
							@can('almacen.articulo.edit')
								<a href="{{URL::action('ArticuloController@edit' , $art->id)}}"><button class="btn btn-info">Editar</button></a>
							@endcan
					
							@can('almacen.articulo.delete')
								<a href="" data-target ="#modal-delete-{{$art->id}}" data-toggle = "modal"><button class="btn btn-danger">Eliminar</button></a>
							@endcan
							</div>
						</td>
					</tr>
					@include('almacen.articulo.modal')
					@endforeach
				</table>

			</div>
			{{$articulos->render()}}</center>
		</div>
	</div>
	
@endsection